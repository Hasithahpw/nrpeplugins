#!/usr/bin/python
import requests
import json
import sys
import time

api_name = sys.argv[1]    #command line argument to get API name
resource_path = sys.argv[2]  # command line argument to get API resource path
service_time_gte = sys.argv[3] #command line argument to get lower limit of API response time
warning_threshold = sys.argv[4] # command line argument to get warning threshold
critical_threshold = sys.argv[5] # command line argument to get critical threshold

current_time = int(time.time()) # get current time in epoch format
ONE_HOUR = 3600000 # define an hour in miliseconds
timestamp_lte = current_time * 1000
timestamp_gte = (current_time * 1000) - ONE_HOUR
url = 'http://<IP>:<PORT>/_search' # elasticsearch url

#elastic search payload
data = {"size":0,"query":{"bool":{"must":[{"range":{"@timestamp":{"gte":timestamp_gte,"lte":timestamp_lte,"format":"epoch_millis"}}},{"match_phrase":{"resourcePath":{"query":resource_path}}},{"match_phrase":{"api":{"query":api_name}}},{"range":{"service-time":{"gte":service_time_gte,"lt":500000}}}]}}}
headers = {'Content-Type': 'application/json'}
response = requests.post(url, json.dumps(data), headers=headers)


json_response = response.json()
delayed_count = json_response['hits']['total']

#define thresholds
if ( int(delayed_count) >= int(critical_threshold) ):
	print "CRITICAL -" + api_name + "-" + resource_path + " - " + str(delayed_count) + " requests has been delayed(>"+ service_time_gte +"ms) within last hour "
	sys.exit(2)
elif ( int(delayed_count) > int(warning_threshold) ):
	print "WARNING -" + api_name + "-" + resource_path + " - " + str(delayed_count) + " requests has been delayed(>"+ service_time_gte +"ms) within last hour "
	sys.exit(1)
elif ( int(delayed_count) <= int(warning_threshold) ):
	print "OK -" + api_name + "-" + resource_path + " - " + str(delayed_count) + " requests has been delayed(>"+ service_time_gte +"ms) within last hour "
	sys.exit(0)
else:
	print "Unknown"
	sys.exit(3)